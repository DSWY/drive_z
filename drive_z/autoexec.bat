@echo off
rem autoexec.bat for DOSEMU + FreeDOS
path z:\bin;z:\gnu;z:\dosemu
set HELPPATH=z:\help
set TEMP=z:\tmp
prompt $P$G
unix -s FUNKID
unix -s HOSTIP
unix -s STARTLAB
unix -s STARTFILE
unix -s DHLDEBUG
unix -s DOSEMU_VERSION
echo "Welcome to dosemu %DOSEMU_VERSION%!"
unix -e
if not exist c:\dhl_mpc.bat xcopy z:\DHLDISK c:\ /E /Q /I /Y
if "%DHLDEBUG%" == "1" goto test1
if "%DHLDEBUG%" == "2" goto end
c:\dhl_mpc.bat
goto end

:test1

\bat\startbat.bat

:end
