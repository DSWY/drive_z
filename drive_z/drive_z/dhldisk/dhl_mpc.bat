@ECHO OFF
REM derived from Autoexec.mpc
PATH c:\network;c:\stapler;C:\bat;z:\bin;z:\gnu;z:\dosemu

SET TEMP=c:\tmp
SET TMP=c:\tmp
PROMPT $P$G

:normal
rem call test.bat
del c:\*.chk
del c:\*.log
rem call network.bat

set pctcp=c:\network\pctcp.ini
echo %HOSTIP%  tandem > c:\network\hosts
ethdrv

cd \stapler
ftp -u file.trans slshup tandem get %STARTFILE% ftpbat.bat
call ftpbat.bat
call startbat.bat
goto ende

:ende
